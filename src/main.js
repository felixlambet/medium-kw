import Vue from 'vue'
import cors from 'cors'
import VueProgress from 'vue-progress-path'
import wysiwyg from 'vue-wysiwyg'
import BootstrapVue from 'bootstrap-vue'
import Vuelidate from 'vuelidate'
import App from './App.vue'
import router from './router'
import store from './store'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vue-progress-path/dist/vue-progress-path.css'

Vue.use(BootstrapVue)
Vue.use(Vuelidate)
Vue.use(cors)
Vue.use(wysiwyg)
Vue.use(VueProgress)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
