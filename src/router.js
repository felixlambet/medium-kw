import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/Home.vue'
import CreatePost from '@/views/CreatePost.vue'
import Posts from '@/views/Posts.vue'
import Read from '@/views/Read.vue'

Vue.use(Router)
Vue.component('router-link', Vue.options.components.RouterLink)
Vue.component('router-view', Vue.options.components.RouterView)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/read/:id',
      component: Read,
      name: 'Read'
    },
    {
      path: '/post/create',
      component: CreatePost
    },
    {
      path: '/posts',
      component: Posts
    },
    {
      path: '/posts/:id',
      component: Posts
    },
    {
      path: '/',
      component: Home
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
});

router.beforeEach((to, from, next) => {
  const publicPages = ['/']
  const authRequired = !publicPages.includes(to.path)
  const loggedIn = localStorage.getItem('userSession')
  if (authRequired && !loggedIn) {
    return next('/')
  }
  return next()
})

export default router
